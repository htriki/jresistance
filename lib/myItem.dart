
import 'package:flutter/material.dart';
import 'package:jresistance/myItemValue.dart';

class MyItem extends DropdownMenuItem {

  MyItem(MyItemValue myItemValue): super(

    value: myItemValue,
    child: Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Icon(Icons.brightness_1_rounded, color: myItemValue.color, size: 35),
          Text(myItemValue.text, style: TextStyle(color: myItemValue.color),),
        ],
      ),
    ),
  );
}