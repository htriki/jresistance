
import 'package:flutter/material.dart';

class MyItemValue {

  String value;
  String text;
  Color color;

  MyItemValue(this.value, this.text, this.color);

  @override
  bool operator ==(other) {
    return (other is MyItemValue)
        && other.value == value;
  }
}