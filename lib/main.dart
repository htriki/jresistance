
import 'package:flutter/material.dart';
import 'package:jresistance/myCouleur.dart';
import 'package:jresistance/myItemValue.dart';
import 'package:jresistance/resistance.dart';

void main() {
  runApp(MyApp());
}


/**
 * Hocine Triki * 2021
 */
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'JResistance'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<MyItemValue> tab = [
    MyItemValue("0", "Noir",   Colors.black),
    MyItemValue("0", "Noir",   Colors.black),
    MyItemValue("0", "Noir",   Colors.black),
    MyItemValue("0", "Noir",   Colors.black),
    MyItemValue("0", "Noir",   Colors.black),
    MyItemValue("0", "Noir",   Colors.black)];

  changeCouleur(index, value) {

    setState(() {
      tab[index] = value;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("Yo"),
            Resistance(tab[0].color, tab[1].color),

            MyCouleur(0, tab[0], changeCouleur),
            MyCouleur(1, tab[1], changeCouleur),
            MyCouleur(2, tab[2], changeCouleur),
            MyCouleur(3, tab[3], changeCouleur),
            MyCouleur(4, tab[4], changeCouleur),
            MyCouleur(5, tab[5], changeCouleur),

          ],
        ),
      ),
    );
  }
}

