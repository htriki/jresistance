

import 'package:flutter/material.dart';
import 'package:jresistance/myItemValue.dart';

import 'myItem.dart';

/**
 * Hocine Triki * 2021
 */
class MyCouleur extends StatelessWidget {

  int index;
  MyItemValue value;
  Function changeCouleur;

  MyCouleur(this.index, this.value, this.changeCouleur);

  @override
  Widget build(BuildContext context) {

    return
    Container(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.white,
          border: Border.all()),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
            value: this.value,
            items: [
              MyItem(MyItemValue("0", "Noir",   Colors.black)),
              MyItem(MyItemValue("1", "Marron", Colors.brown)),
              MyItem(MyItemValue("2", "Rouge",  Colors.red)),
              MyItem(MyItemValue("3", "Orange",  Colors.orange)),
              MyItem(MyItemValue("4", "Jaune",  Colors.yellow)),
              MyItem(MyItemValue("5", "Vert",  Colors.green)),
              MyItem(MyItemValue("6", "Bleu",  Colors.blue)),
              MyItem(MyItemValue("7", "Violet",  Colors.deepPurple)),
              MyItem(MyItemValue("8", "Gris",   Colors.grey)),
              MyItem(MyItemValue("9", "Blanc",  Colors.white)),
            ],
            onChanged: (value) {
              this.changeCouleur(this.index, value);
            }),
      ),
    );

  }
}