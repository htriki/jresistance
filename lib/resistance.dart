

import 'package:flutter/material.dart';
import 'dart:ui';
import 'dart:ui' as ui;

class Resistance extends StatelessWidget {

  final size = (window.physicalSize) / (window.devicePixelRatio);
  Color _colorBand1;
  Color _colorBand2;


  Resistance(this._colorBand1,  this._colorBand2);

  @override
  Widget build(BuildContext context) {

    var newSize = Size(300, 80);

    return CustomPaint(
      size: newSize, // Size(300, 200),
      painter: Painter(this._colorBand1, this._colorBand2),
    );
  }
}

/**
 * Le dessin -> R1  R2  R3  R2  R1
 */
class Painter extends CustomPainter {

  final Size sizeR1 = Size(30, 10);
  final Size sizeR2 = Size(60, 50);
  final Size sizeR3 = Size(110, 40);

  Color _colorBand1;
  Color _colorBand2;
  Color _colorBand3;
  Color _colorBand4;
  Color _colorBand5;
  Color _colorBand6;

  Painter(this._colorBand1, this._colorBand2);

  var paint1 = Paint()
    ..color = Colors.amberAccent[100];
    //..style = PaintingStyle.stroke;

  var paint2 = Paint()
    ..color = Colors.red;

  var paintBand1 = Paint();
  var paintBand2 = Paint();

  @override
  void paint(Canvas canvas, Size size) {

    paintBand1.color = _colorBand1;
    paintBand2.color = _colorBand2;



    // R1
    canvas.drawRect(Offset(0, 20) & sizeR1, paint2);

    // R2
    canvas.drawRect(Offset(sizeR1.width, 0) & sizeR2, paint1);

    // R3
    canvas.drawRect(Offset(sizeR1.width + sizeR2.width, 5) & sizeR3, paint1);

    // R2
    canvas.drawRect(Offset(sizeR1.width + sizeR2.width + sizeR3.width, 0) & sizeR2, paint1);

    // R1
    canvas.drawRect(Offset(sizeR1.width + (sizeR2.width * 2) + sizeR3.width, 20) & sizeR1, paint2);

    //Les bandes
    canvas.drawRect(Offset(sizeR1.width + 25 , 0) & Size(10, 50), paintBand1);
    canvas.drawRect(Offset((sizeR1.width + sizeR2.width) , 5) & Size(10, 40), paintBand2);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class ImagePainter extends CustomPainter {

  ui.Image image;

  ImagePainter({this.image});

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawImage(image, Offset.zero, Paint());
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}